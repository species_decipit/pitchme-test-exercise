from django.urls import path, include

from server.apps.main.api.v1 import urls

app_name = 'main'

urlpatterns = [
    path('v1/', include(urls)),
]
