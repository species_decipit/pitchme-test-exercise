from typing import List

from django.core.mail import send_mail
from dramatiq import actor


@actor
def send_async_mail(
    subject: str,
    message: str,
    from_email: str,
    recipient: List[str],
    fail_silently: bool = False,
) -> int:
    """Method asynchronously send mails.

    Args:
        subject: Topic of mail
        message: Body of mail
        from_email: From
        recipient: List of recipients
        fail_silently: Propagate error or not

    Returns:
        Number of successfully sent mails

    """
    return send_mail(subject, message, from_email, recipient, fail_silently)
