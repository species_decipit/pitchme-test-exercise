import logging

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import m2m_changed
from django.dispatch import receiver
from server.apps.main.tasks import send_async_mail

from server.apps.main.models import Event

logger = logging.getLogger('django')


@receiver(m2m_changed, sender=Event.topics.through)
def send_notification_to_users(instance: Event, action: str, reverse: bool, **kwargs):
    """Function sends emails to users if suitable event has been created.

    Args:
        instance: The actual instance being saved
        action: A string indicating the type of update that is done on the relation
        reverse: Indicates which side of the relation is updated
        **kwargs:

    """
    if action == 'post_add' and not reverse:
        target_users = User.objects.filter(
            user_event_filters__location__contains=instance.location.location,
            user_event_filters__start_from__lte=instance.start,
            user_event_filters__start_until__gte=instance.start,
            user_event_filters__topics__topic__in=instance.topics.values_list('topic', flat=True),
        ).distinct('id')

        send_async_mail.send(
            **{
                'subject': 'New event was created',
                'message': f'Event {instance.title} was added',
                'from_email': settings.EMAIL_HOST_USER,
                'recipient': list(target_users.values_list('email', flat=True)),
            },
        )
