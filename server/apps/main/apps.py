from django.apps import AppConfig


class Main(AppConfig):
    name = 'server.apps.main'

    def ready(self):
        from server.apps.main import signals
