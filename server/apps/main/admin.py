from django.contrib import admin

from server.apps.main.models import *


@admin.register(Location)
class BlogPostAdmin(admin.ModelAdmin):
    pass


@admin.register(Event)
class BlogPostAdmin(admin.ModelAdmin):
    pass


@admin.register(Topic)
class BlogPostAdmin(admin.ModelAdmin):
    pass


@admin.register(UserEventFilter)
class BlogPostAdmin(admin.ModelAdmin):
    pass
