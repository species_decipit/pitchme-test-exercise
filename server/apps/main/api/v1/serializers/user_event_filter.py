from typing import Dict, Any

from rest_framework.serializers import ModelSerializer

from server.apps.main.api.v1.serializers.topic import GetTopicSerializer
from server.apps.main.models import UserEventFilter


class BaseUserEventFilterSerializer(ModelSerializer):
    """Base serializer for UserEventFilter model."""

    class Meta:
        model = UserEventFilter
        fields = ('id', 'location', 'topics', 'start_from', 'start_until')


class GetUserEventFilterSerializer(BaseUserEventFilterSerializer):
    """GET serializer for UserEventFilter model."""

    topics = GetTopicSerializer(many=True)


class PostUserEventFilterSerializer(BaseUserEventFilterSerializer):
    """POST serializer for UserEventFilter model."""

    def create(self, validated_data: Dict[str, Any]) -> UserEventFilter:
        validated_data['user_id'] = self.context['request'].user.id
        return super().create(validated_data)
