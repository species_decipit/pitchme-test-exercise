from rest_framework.serializers import ModelSerializer

from server.apps.main.models import Topic


class BaseTopicSerializer(ModelSerializer):
    """Base serializer for Topic model"""

    class Meta:
        model = Topic
        fields = ('id', 'topic')


class GetTopicSerializer(BaseTopicSerializer):
    """GET serializer for Topic model"""
