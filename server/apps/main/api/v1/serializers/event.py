from rest_framework.serializers import ModelSerializer

from server.apps.main.api.v1.serializers.location import (
    GetLocationSerializer,
)
from server.apps.main.api.v1.serializers.topic import GetTopicSerializer
from server.apps.main.models import Event


class BaseEventSerializer(ModelSerializer):
    """Base serializer for Event model."""

    class Meta:
        model = Event
        fields = ('id', 'title', 'start', 'end', 'location', 'topics')


class GetEventSerializer(BaseEventSerializer):
    """GET serializer for Event model."""

    location = GetLocationSerializer()
    topics = GetTopicSerializer(many=True)
