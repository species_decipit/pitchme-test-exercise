from rest_framework.serializers import ModelSerializer

from server.apps.main.models import Location


class BaseLocationSerializer(ModelSerializer):
    """Base serializer for Location model"""

    class Meta:
        model = Location
        fields = ('id', 'location')


class GetLocationSerializer(BaseLocationSerializer):
    """GET serializer for Location model"""
