import operator
from functools import reduce

from django.db.models import Q
from django_filters import FilterSet, CharFilter, Filter, DateTimeFromToRangeFilter

from server.apps.main.models import Event


class ListFilter(Filter):
    def filter(self, qs, value):
        value_list = value.split(u',')
        query = reduce(
            operator.and_,
            (
                Q(**{f'{self.field_name}__{self.lookup_expr}': value.strip()})
                for value in value_list
            ),
        )
        return qs.exclude(~query)


class EventFilter(FilterSet):
    """Class describes filters for Event model."""

    location = CharFilter(lookup_expr='location__contains')
    topics = ListFilter(field_name='topics__topic', lookup_expr='icontains')
    start = DateTimeFromToRangeFilter()

    class Meta:
        model = Event
        fields = ()
