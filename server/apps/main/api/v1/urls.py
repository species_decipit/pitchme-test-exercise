from django.urls import path, include
from rest_framework.routers import DefaultRouter

from server.apps.main.api.v1.views import event as event_views
from server.apps.main.api.v1.views import user_event_filter as user_event_filter_views

router = DefaultRouter()
router.register(r'events', event_views.EventViewSet, basename='Event')
router.register(
    r'user_event_filters',
    user_event_filter_views.UserEventFilterViewSet,
    basename='UserEventFilter',
)

urlpatterns = [
    path('', include(router.urls)),
]
