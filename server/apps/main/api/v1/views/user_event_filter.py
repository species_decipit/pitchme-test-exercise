from typing import Dict, Type

from django.db.models import QuerySet
from rest_framework.permissions import IsAuthenticated
from rest_framework.serializers import ModelSerializer
from rest_framework.viewsets import ModelViewSet

from server.apps.main.api.v1.serializers.event import GetEventSerializer
from server.apps.main.api.v1.serializers.user_event_filter import (
    GetUserEventFilterSerializer,
    PostUserEventFilterSerializer,
)
from server.apps.main.models import UserEventFilter


class UserEventFilterViewSet(ModelViewSet):
    """View provides the following endpoints:
    GET /v1/user_event_filters/
    POST /v1/user_event_filters/
    DELETE /v1/user_event_filters/{user_event_filter_id}/
    PATCH /v1/user_event_filters/{user_event_filter_id}/
    """

    serializers_mapping: Dict[str, Type[ModelSerializer]] = {
        'GET': GetUserEventFilterSerializer,
        'POST': PostUserEventFilterSerializer,
    }
    http_method_names = ('get', 'post', 'delete')
    serializer_class = GetEventSerializer
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self) -> Type[ModelSerializer]:
        """Method returns the class to use for the serializer."""
        return self.serializers_mapping[self.request.method]

    def get_queryset(self) -> QuerySet[UserEventFilter]:
        return UserEventFilter.objects.filter(user=self.request.user)
