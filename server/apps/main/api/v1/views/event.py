from rest_framework.viewsets import ModelViewSet

from server.apps.main.api.v1.filters.event import EventFilter
from server.apps.main.api.v1.serializers.event import GetEventSerializer
from server.apps.main.models import Event


class EventViewSet(ModelViewSet):
    """View provides the following endpoints:
        GET /v1/events/
    """

    http_method_names = ('get',)
    queryset = Event.objects.all()
    serializer_class = GetEventSerializer
    filterset_class = EventFilter
