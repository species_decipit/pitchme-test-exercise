from django.contrib.auth.models import User
from django.db.models import Model, ForeignKey, CASCADE, CharField, ManyToManyField, DateTimeField


class UserEventFilter(Model):
    """Table contains user filters for events"""

    user = ForeignKey(User, on_delete=CASCADE, related_name='user_event_filters')
    location = CharField(max_length=512, null=True, blank=True)
    topics = ManyToManyField('Topic', related_name='user_event_filters')
    start_from = DateTimeField(null=True, blank=True)
    start_until = DateTimeField(null=True, blank=True)
