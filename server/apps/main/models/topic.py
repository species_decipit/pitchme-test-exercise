from django.db.models import (
    Model,
    CharField,
)


class Topic(Model):
    """Table contains topics for conversations during events."""

    topic = CharField(max_length=128)
