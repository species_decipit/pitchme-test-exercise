from django.db.models import (
    Model,
    CharField,
    DateTimeField,
    ManyToManyField,
    DO_NOTHING,
    ForeignKey,
)


class Event(Model):
    """Table contains events."""

    title = CharField(max_length=128)
    start = DateTimeField()
    end = DateTimeField()

    topics = ManyToManyField('Topic', related_name='events')
    location = ForeignKey('Location', on_delete=DO_NOTHING, related_name='events')
