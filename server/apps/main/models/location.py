from django.db.models import (
    Model,
    CharField,
)


class Location(Model):
    """Table contains locations of events."""

    location = CharField(max_length=512)
